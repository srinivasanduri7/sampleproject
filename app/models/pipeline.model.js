module.exports = (sequelize, Sequelize) => {
    const Pipeline = sequelize.define("pipeline", {
      interface: {
        type: Sequelize.STRING
      },
      currentStatus: {
        type: Sequelize.STRING
      },
      startTime: {
        type: Sequelize.STRING
      },
      endTime: {
        type: Sequelize.STRING
      },
      frequency: {
        type: Sequelize.STRING
      }
    });
  
    return Pipeline;
  };
  