const db = require("../models");
const Pipeline = db.pipeline;
const Op = db.Sequelize.Op;


// Create and Save a new Pipeline
exports.create = (req, res) => {
    // Validate request
    if (!req.body.interface) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
  
    // Create a Pipeline
    const pipeline = {
      interface: req.body.interface,
      currentStatus: req.body.currentStatus,
      startTime: req.body.startTime,
      endTime: req.body.endTime,
      frequency: req.body.frequency 
    };
  
    // Save pipeline in the database
    Pipeline.create(pipeline)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Pipeline."
        });
      });
  };
  
  // Retrieve all Pipelines from the database.
  exports.findAll = (req, res) => {
    const title = req.query.title;
    
  
    Pipeline.findAll({})
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving pipelines."
        });
      });
  };