module.exports = app => {
    const pipeline = require("../controllers/pipeline.controller.js");
  
    var router = require("express").Router();
  
    // Create a new pipeleine
    router.post("/", pipeline.create);
  
    // Retrieve all pipelines
    router.get("/", pipeline.findAll);
  
    app.use('/api/pipeline', router);
  };
  